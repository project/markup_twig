<?php

namespace Drupal\markup_twig;

use Drupal\Core\Security\TrustedCallbackInterface;

class MarkupTwigHelpers implements TrustedCallbackInterface {

  /**
   * Returns the render array for inline_template.
   * See https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!InlineTemplate.php/class/InlineTemplate
   *
   * @param string $value The text value / template.
   * @param string $format The input format selected.
   * @param string $langcode The langcode.
   * @param array $context The twig context array.
   * @return array
   */
  public static function buildElementInlineTemplate($value, $format, $langcode = '', array $context = array()) {
    return [
      // See https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!InlineTemplate.php/class/InlineTemplate/
      '#type' => 'inline_template',
      '#template' => $value,
      '#context' => $context,
      // Run value through selected filter type after rendering it.
      '#post_render' => [[self::class, 'applyTextFormatFiltersInPostRender']],
      '#markup_twig_format' => $format,
      '#markup_twig_langcode' => $langcode,
    ];
  }

  /**
   * Returns global twig variables (context).
   *
   * @return array Global twig context
   */
  public static function getTwigGlobalContext() {
    global $base_url;

    $context = [];

    $theme = \Drupal::theme()->getActiveTheme();
    $context['theme'] = $theme->getName();
    $context['theme_directory'] = $theme->getPath();

    $context['base_path'] = base_path();
    $context['base_root'] = $base_url;
    $context['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
    $context['language'] = \Drupal::languageManager()->getCurrentLanguage();

    $user = \Drupal::currentUser();
    $context['is_admin'] = $user->hasPermission('access administration pages');
    $context['logged_in'] = $user->isAuthenticated();

    $site_config = \Drupal::config('system.site');
    $context['site_name'] = strip_tags($site_config->get('name'));
    $context['site_slogan'] = strip_tags($site_config->get('slogan'));

    return $context;
  }

  /**
   * @inheritDoc
   */
  public static function trustedCallbacks() {
    return ['applyTextFormatFiltersInPostRender'];
  }

  public static function applyTextFormatFiltersInPostRender ($rendered, $element) {
    return check_markup($rendered, $element['#markup_twig_format'], $element['#markup_twig_langcode']);
  }

}
